<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Location extends Model{
    protected $fillable = [
        'avance', 'reste', 'date_location', 'assurance', 'notes', 'event'
    ];

    private static $time_intervals = [
        "0" => "Aujourd'hui",
        "-1" => "Hier",
        "1" => "Demain",
        "6" => "Dans une semaine"
    ];

    public static function getList($key, $name = "", $selected_values = []){
        return [
            'categories' => Location::$$key,
            'name' => $name,
            'selected_values' => $selected_values
        ];
    }

    public function robe(){
        return $this->belongsTo('App\Robe');
    }

    public function user(){
        return $this->belongsTo("App\User");
    }

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    public function addons(){
        return $this->belongsToMany('App\Addon');
    }

    public function scopeDate($query, $begin_date, $end_date){
        return $query->whereBetween('date_location', [$begin_date, $end_date])->orderBy('date_location');
    }


    public function setDateLocationAttribute($value){
        Carbon::setLocale('fr');
        $this->attributes['date_location'] = Carbon::createFromFormat('Y-m-d H:i', $value);
    }


    public function getDateLocationAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }


    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d H:i');
    }
}
