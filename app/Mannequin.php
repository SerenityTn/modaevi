<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mannequin extends Model{
    protected $guarded = ['xp_shoot', 'xp_def'];


    public function imgs(){
        return $this->hasMany('App\MannequinImg');
    }
}
