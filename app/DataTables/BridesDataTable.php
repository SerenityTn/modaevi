<?php

namespace App\DataTables;

use App\Bride;
use Yajra\Datatables\Services\DataTable;

class BridesDataTable extends DataTable{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax(){
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('control', function ($model) {
                $type = 'bride';
                return (string)view("partials.control", compact('model','type'));
            })
            ->editColumn('personal_img', '<img src='. url('images/brides_imgs/{{$personal_img}}/150') .'/>')
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(){
        $brides = Bride::select()->latest();
        return $this->applyScopes($brides);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'personal_img',
            'name',
            'wedding_date',
            'phone',
            'bride_budget',
            'wtiya_budget',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'bridesdatatables_' . time();
    }
}
