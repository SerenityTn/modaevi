<?php

namespace App\DataTables;

use Yajra\Datatables\Services\DataTable;
use App\Mannequin;

class MannequinsDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax(){
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('control', function ($model) {
                $type = 'mannequin';
                return (string)view("partials.control", compact('model', 'type'));
            })
            ->addColumn('personal_img', function($model){
                if($model->imgs()->where('type','0')->first()){
                    $name = $model->imgs()->where("type","0")->first()->name;
                    $path = url('images/mannequins_imgs/'. $name.'/150');
                    return '<img src='. $path .'/>';
                }else
                    return "No Image";
            })
            ->editColumn('cv_file', function($model){
                $cv_path = url('file/'.$model->cv_file);
                if(empty($model->cv_file))
                    return "Pas de CV";
                else
                    return "<a href='".route("download", ["name" => $model->cv_file])."'>Télécharger</a>";
            })
            ->editColumn('xp_shoot_txt', function($model){
                return empty($model->xp_shoot_txt) ? "Pas d'éxpérience." : $model->xp_shoot_txt;
            })
            ->editColumn('xp_def_txt', function($model){
                return empty($model->xp_def_txt) ? "Pas d'éxpérience." : $model->xp_def_txt;
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(){
        $mannequins = Mannequin::select()->latest();
        return $this->applyScopes($mannequins);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html(){
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
     protected function getColumns()
     {
         return [
             'id',
             'name',
             'city',
             'age',
             'email',
             'phone',
             'height',
             'weight',
             'xp_def_txt',
             'shoot_txt',
             'cv_file',
             'notice',
             'created_at',
             'updated_at',
         ];
     }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mannequinsdatatables_' . time();
    }
}
