<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Location;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Datatables;

class LocationsDataTable extends DataTable{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax(){
        $request = request();
        $begin_date = request()->input("begin_date");
        if(request()->has("end_date"))
            $end_date = request()->input("end_date");
        else
            $end_date = "2020-01-01 00:00";

        $locations = $this->query()->whereBetween('date_location', [$begin_date, $end_date]);
        return Datatables::of($locations)->filter(function ($query) use ($request){
            if ($request->has('name') && !empty($request->input('name')))
                $query->where('clientes.name', 'like', "%{$request->get('name')}%");

            if ($request->has('robe') && !empty($request->input('robe')))
                $query->where('robes.ref', '=', $request->get('robe'));

            if($request->has('areas'))
                $query->where('user_id', '=', $request->input('areas'));

        })
        ->addColumn('control', function ($model) {
            return (string)view("control", compact('model'));
        })
        ->editColumn('etat', '<img src='.asset(URL::asset('imgs/icons/state{{$etat}}.png')) . ' width="30" class="state"/>')
        ->editColumn('ref', '{{ $ref == -1 ? "Coiffure": $ref  - App\Robe::$categories[$category] }}')
        ->editColumn('name', function($model){
            return "<span data-toggle='tooltip' title='Numéro: ".$model->number1."'>".$model->name."</span>";
        })
        ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(){
        $locations = Location::query()
            ->join('users', 'users.id', '=', 'locations.user_id')
            ->join('clientes', 'clientes.id', '=', 'locations.cliente_id')
            ->join('robes', 'robes.id', '=', 'locations.robe_id')
            ->select(['locations.*','users.name as username','clientes.name',
            'clientes.number1', 'clientes.number2', 'robes.ref','robes.etat', 'robes.category']);
        return $locations;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'user_id',
            'cliente_id',
            'robe_id',
            'avance',
            'reste',
            'date_location',
            'assurance',
            'notes',
            'etat',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'locationsdatatables_' . time();
    }
}
