<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Bride extends Model{
    protected $guarded = ['id', 'coif_bride', 'robe_bride', 'coif_wtiya', 'robe_wtiya', 'hijab'];

    public function prefs(){
        return $this->belongsToMany('App\Pref');
    }

    public function imgs(){
        return $this->hasMany('App\Img');
    }

    public function setWeddingDateAttribute($input){
        $format = 'd-m-Y';
        $this->attributes['wedding_date'] = Carbon::createFromFormat($format, $input);
    }

    
}
