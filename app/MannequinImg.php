<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MannequinImg extends Model{
    protected $guarded = [];
    public static $types = ["portrait" => 0];
    public function mannequin(){
        return $this->belongsTo('App\Mannequin');
    }
}
