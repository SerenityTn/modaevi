<?php

Route::get('/', function () {
    return view('index');
});

Route::get('images/{root}/{filename}/{width}/{height?}', function($root, $filename, $width, $height=null){
    $img = Image::make(storage_path($root."/".$filename));
    $img->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
    });
    return $img->response();
});


Route::get('public/images/{filename}/{width}/{height}', function($filename, $width, $height){
    $img = Image::make(public_path("imgs/robes/$filename"));
    $img->resize($width, $height);
    return $img->response();
});

Route::group(['prefix' => '{locale}'], function(){
    Route::get("bride-form/create",[
        "as" => "bride-form.create",
        "uses" => "BridesController@create",
    ]);

    Route::get('mannequin-form/create',[
        "as" => "mannequin-form.create",
        "uses" => "MannequinsController@create"
    ]);

    Route::post('bride-form/data-check', [
        "as" => 'bride-form.check',
        "uses" => "BridesController@data_check"
    ]);

    Route::post('mannequin-form/data-check', [
        "as" => 'mannequin-form.check',
        "uses" => "MannequinsController@data_check"
    ]);

});

Route::post("bride-form/create",[
    "as" => "bride-form.store",
    "uses" => "BridesController@store",
]);

Route::post('mannequin-form/create',[
    "as" => "mannequin-form.store",
    "uses" => "MannequinsController@store"
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('file/{name}', ['as' => 'file', function($name){        
        return response()->file(storage_path('cvs/'.$name));
    }]);

    Route::get('download/{name}', ['as' => 'download', function($name){
        return response()->download(storage_path('cvs/'.$name));
    }]);

    Route::resource('bride', 'BridesController',['only' => [
        'index','destroy', 'show'
    ]]);

    Route::resource('mannequin', 'MannequinsController',['only' => [
        'index','show', 'destroy'
    ]]);

    Route::controller('bride/data', 'BridesController', [
        'getBridesDatatable' => 'brides.data'
    ]);

    Route::controller('mannequin/data', 'MannequinsController', [
        'getMannequinsDatatable' => 'mannequins.data'
    ]);


    Route::get('/', [
        'as' => 'admin.index',
        'uses' => 'DashboardController@index'
    ]);

    Route::resource('robe', 'RobesController');

    Route::resource('cliente', 'ClientesController');

    Route::get('location/cliente/{cliente_id}/create', [
        'as' => 'admin.location.cliente.create',
        'uses' => 'LocationsController@createFromCliente'
    ]);

    Route::post('location/cliente/{cliente_id}/filter_list', [
        'as' => 'admin.robe.filter_list',
        'uses' => 'RobesController@filter_list'
    ]);

    Route::post('robe/filter', [
        'as' => 'admin.robe.filter',
        'uses' => 'RobesController@filter'
    ]);

    Route::post('robe/filter_list', [
        'as' => 'admin.robe.filter_list',
        'uses' => 'RobesController@filter_list'
    ]);

    Route::get('robe/update_state/{robe}', [
        'as' => 'admin.robe.update_state',
        'uses' => 'RobesController@update_state'
    ]);

    Route::get('robe/update_current_local/{robe}', [
        'as' => 'admin.robe.update_current_local',
        'uses' => 'RobesController@update_current_local'
    ]);

    Route::get('location/robe/{robe_id}/create', [
        'as' => 'admin.location.robe.create',
        'uses' => 'LocationsController@createFromRobe'
    ]);

    Route::controller('location/filter', 'LocationsController', [
        'postFilterLocation' => 'locationDatatable.filter'
    ]);

    Route::controller('cliente/data', 'ClientesController', [
        'getClientesDatatable' => 'clientes.data'
    ]);

    Route::controller('locations', 'LocationsController', [
        'getLocationDatatable'  => 'datatables.data',
        'getIndex' => 'datatables',
    ]);

    Route::get('location/datatable', [
        'as' => 'admin.location.datatable',
        'uses' => 'LocationsController@getLocations'
    ]);

    Route::resource('location', 'LocationsController');
});

Route::auth();

Route::get('/home', 'HomeController@index');
