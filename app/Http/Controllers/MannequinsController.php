<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use App\DataTables\MannequinsDataTable;
use App\MannequinImg;
use App\Mannequin;

class MannequinsController extends Controller{
    protected $destinationPath;
    protected $cv_path;
    public function index(){
        return view('mannequins.index');
    }

    public function create(){
        return view('public.mannequin-form');
    }

    public function store(){
        $this->destinationPath = storage_path('/mannequins_imgs');
        $this->cv_path = storage_path('/cvs');
        $params = request()->input();
        if(request()->hasFile('cv_file')) $this->cv_handler($params);
        $mannequin = Mannequin::create($params);
        $this->img_handler($mannequin);
        return view('public.success', ['msg'=>'Merci! Votre inscription est bien prise en compte.']);
    }

    public function cv_handler(&$params){
        $cv_file = request()->file('cv_file');
        $cv_name = request()->input('cin')."_cv.".$cv_file->getClientOriginalExtension();
        $params['cv_file'] = $cv_name;
        $cv_file->move($this->cv_path, $cv_name);
    }

    public function img_handler($mannequin){
        $types =['portrait'];
        $all_imgs = array();
        foreach($types as $type){
            for($i = 0; $i < 3; $i++){
                $file = $type.$i;
                if(request()->file($file) != null){
                    $this->assign_imgs(request()->file($file), $type, $i, $mannequin);
                }
            }
        }
    }

    public function assign_imgs($img, $type, $i, $mannequin){
        $img_name = $mannequin->cin . "_".$type."_" .$i. "." .$img->getClientOriginalExtension();
        $image = new MannequinImg(['name' => $img_name, 'type' => MannequinImg::$types[$type]]);
        $img->move($this->destinationPath, $img_name);
        $mannequin->imgs()->save($image);
    }

    public function data_check(){
        $allrules = [
            "cin"   => "required|min:8",
            "email" => "required|email",
            "name"  => "required|min:4",
            "phone" => "required|numeric|min:8",
            "city" => "not_in:0",
            "age" => "required|numeric",
            "height" => "required|numeric",
            "weight" => "required|numeric",
            "xp_shoot" => "required",
            "xp_def" => "required",
            "xp_shoot_txt" => 'required_if:xp_shoot,1|min:40',
            "xp_def_txt" => 'required_if:xp_def,1',
            "portrait0" => "required"
        ];

        $rules = array();
        foreach($allrules as $key => $value){
            if(array_key_exists($key, request()->input()))
                $rules[$key] = $value;
        }

        $validator = Validator::make(request()->input(), $rules);
        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }
        return response()->json(array('success' => true), 200);
    }

    public function show($id){
        $mannequin = Mannequin::find($id);
        return view('mannequins.show', compact('mannequin'));
    }

    public function getMannequinsDatatable(MannequinsDataTable $mannequinDataTable){
        return $mannequinDataTable->render("mannequins");
    }

    public function destroy($id){
        $mannequin = Mannequin::find($id);
        $mannequin->delete();
        return redirect()->to(route('admin.mannequin.index'));
    }
}
