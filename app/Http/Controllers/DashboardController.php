<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Location;
use App\Cliente;
use App\Http\Requests;

class DashboardController extends Controller{
    public function index(){        
        return view('admin.index', compact('locations'));
    }
}
