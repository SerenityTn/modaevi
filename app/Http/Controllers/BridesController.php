<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Bride;
use App\Http\Requests;
use App\Img;
use App\Pref;
use Illuminate\Support\Facades\Validator;
use App\DataTables\BridesDataTable;
use Illuminate\Support\Facades\Input;

use Hash;

class BridesController extends Controller{
    protected $destinationPath;

    public function index(){
        $brides = Bride::paginate(10);
        return view('brides.index', compact('brides'));
    }

    public function show($id){
        $bride = Bride::find($id);
        return view('brides.show', compact('bride'));
    }

    public function getBridesDatatable(BridesDataTable $brideDataTable){
        return $brideDataTable->render("brides");
    }

    public function create(){
        return view('public.bride-form');
    }

    public function data_check(){
        $allrules = [
            "cin"   => "required|min:8",
            "name"  => "required|min:4",
            "email" => "required|email",
            "phone" => "required|numeric|min:8",
            "city" => "not_in:0",
            "wedding_date" => "required",
            "visited_us" => "required",
            "bride_budget" => "required_without:wtiya_budget",
            "wtiya_budget" => "required_without:bride_budget",
            "personal_img" => ["required","regex:/\.(gif|jpg|jpeg|tiff|png)$/i"]
        ];

        $rules = array();
        foreach($allrules as $key => $value){
            if(array_key_exists($key, request()->input()))
                $rules[$key] = $value;
        }

        $validator = Validator::make(request()->input(), $rules);
        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }
        return response()->json(array('success' => true), 200);
    }

    public function store(){
        $this->destinationPath = storage_path('/brides_imgs');
        $params = request()->input();
        $this->personal_img_handler($params);
        $bride = Bride::create($params);
        $this->prefs_handler($bride);
        $this->img_handler($bride);
        return view('public.success', ['msg'=>"Merci chère mariée! Nous serons heureux de vous voir parmi nous !"]);
    }

    public function prefs_handler($bride){
        $prefs = Pref::all();
        foreach($prefs as $pref){
            if(request()->has(str_replace(" ","_",$pref->label))){
                $bride->prefs()->attach($pref);
            }
        }
    }

    public function personal_img_handler(&$params){
        $img = request()->file('personal_img');
        $image_name = request()->input('cin') . '_perso.' . $img->getClientOriginalExtension();
        $params['personal_img'] = $image_name;
        $img->move($this->destinationPath, $image_name);
    }

    public function img_handler($bride){
        $types =['coif', 'make_up', 'robe'];
        $all_imgs = array();
        foreach($types as $type){
            for($i = 0; $i < 3; $i++){
                $file = $type.$i;
                if(request()->file($file) != null){
                    $this->assign_imgs(request()->file($file), $type, $i, $bride);
                }
            }
        }
    }

    public function assign_imgs($img, $type, $i, $bride){
        $img_name = $bride->cin . "_".$type."_" .$i. "." .$img->getClientOriginalExtension();
        $image = new Img(['name' => $img_name, 'type' => Img::$types[$type]]);
        $img->move($this->destinationPath, $img_name);
        $bride->imgs()->save($image);
    }

    public function destroy($id){
        $bride = Bride::find($id);
        $bride->delete();
        return redirect()->to(route('admin.bride.index'));
    }
}
