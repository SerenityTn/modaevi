<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pref extends Model{
    protected $fillable = ["label"];    
    public function brides(){
        return $this->belongsToMany('App\Bride');
    }
}
