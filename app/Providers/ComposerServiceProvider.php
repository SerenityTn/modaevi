<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Addon;
use App\Location;
use App\Robe;
use App\Http\Controllers\LocationsController;
use App\DataTables\LocationsDataTable;

class ComposerServiceProvider extends ServiceProvider{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        view()->composer(['brides.*','mannequins.*', 'public.mannequin-form', 'public.bride-form'] , function($view){
            $cities = ["","Tunis", "Sousse", "Monastir","Nabeul","Kairouan","Mahdia","Sfax", "Béja","Bizerte","Gabès","Gafsa","Jendouba","Kasserine","KébiliLa","Le Kef","Médenine","Sidi Bouzid","Siliana","Tataouine","Tozeur","Zaghouan"];
            $view->with('cities', $cities);
        });

        view()->composer(['brides.show', 'public.bride-form'], function($view){
            $offres_label = \App\Pref::all();
            $offres = ["coif_bride" => "Coiffure Mariée", "coif_wtiya" => "Coiffure Wtiya", "robe_wtiya" => "Robe Wtiya", "robe_bride" => "Robe Mariée", "hijab" => "Voilée"];
            $view->with('offres', $offres)->with('offres_label', $offres_label);
        });

        view()->composer('partials.forms.addons', function($view){
            $view->with('addons', Addon::all());
        });

        view()->composer(['robes.create', 'robes.edit', 'robes.filters', 'robes.list', 'locations.filters'], function($view){
            $color_options = ['Blanche', 'Noire', 'Rose','Semon','Aubergine', 'Dorée', 'Rouge Bordeau', 'Jaune', 'Ecru', 'Rouge', 'Verte', 'Verte doux', 'Bleue', 'Bleue marine'];
            $view->with('color_options', $color_options);
        });

        view()->composer(['robes.categories.dropdown', 'robes.categories.checkbox'], function($view){
            $categories = ['0' => 'Mariée', '1' => 'Fiancaille', '2' => 'Keswa', '3' => 'Hadhara'];
            $view->with('categories', $categories);
        });

        view()->composer(['locations.create', 'locations.filters', 'robes.categories.radio'], 'App\Http\ViewComposers\LocationComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        //
    }
}
