<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;

class RouteServiceProvider extends ServiceProvider{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router){
        //

        parent::boot($router);
        $router->model('robe', 'App\Robe');
        $router->model('cliente', 'App\Cliente');
        $router->model('location', 'App\Location');

    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router, Request $request){
        $this->mapWebRoutes($router);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
            $locale = request()->segment(1);
            $this->app->setLocale($locale);
            $router->group(['prefix' => $locale], function($router) {
                $router->post("bride-form/create",[
                    "as" => "bride-form.store",
                    "uses" => "BridesController@store",
                ]);

                $router->post('bride-form/data-check', [
                    "as" => 'bride-form.check',
                    "uses" => "BridesController@data_check"
                ]);

                $router->get('mannequin-form/create',[
                    "as" => "mannequin-form.create",
                    "uses" => "MannequinsController@create"
                ]);

                $router->post('mannequin-form/data-check', [
                    "as" => 'mannequin-form.check',
                    "uses" => "MannequinsController@data_check"
                ]);
            });
        });
    }
}
