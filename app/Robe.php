<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Robe extends Model{
    protected $fillable = [
        'ref','color','size', 'category', 'buy_price', 'price', 'min_price', 'etat', 'local', 'current_local'
    ];

    public static $categories = ['0' => 'Mariée', '1' => 'Fiancaille', '2'=> 'Keswa', '3' => 'Hadhara'];
    public static $locals = ['0' => 'Sousse', '1' => 'Jemmel'];
    public static $etats = ['0' => 'En boutique', '1' => 'Sortie'];

    public static function getList($key, $name, $selected_values = [0], $default = false){
        return [
            'categories' => Robe::$$key,
            'name' => $name,
            'selected_values' => $selected_values,
            'default' => $default
        ];
    }


    public static function filter($robes){
        if(request()->has('categories'))
            $robes = Robe::filter_category($robes);

        if(request()->has('state'))
            $robes = Robe::filter_state($robes);

        if(request()->has('local'))
            $robes = Robe::filter_local($robes);

        if(request()->has('current_local'))
            $robes = Robe::filter_current_local($robes);
            $request = request();
        if(request()->has('ref'))
            $robes->where('ref','like', "%{$request->input('ref')}%");

        return $robes;
    }

    public static function filter_current_local($robes){
        $current_local = request()->input('current_local');
        return $robes->where('current_local', $current_local);
    }

    public static function filter_local($robes){
        $local = request()->input('local');
        return $robes->where('local', $local);
    }

    public static function filter_state($robes){
        $state = request()->input('state');
        return $robes->where('etat', $state);
    }

    public static function filter_category($robes){
        $categories = request()->input('categories');
        return $robes->whereIn('category', $categories);
    }

    public function locations(){
        return $this->hasMany('App\Location');
    }

    public function getCategoryName(){
        return Robe::$categories[$this->category];
    }
}
