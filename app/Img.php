<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Img extends Model{
    public static $types = ["coif"=> 0,"make_up" => 1, "robe" => 2];    
    protected $fillable = ["name", "type"];

    public function bride(){
        return $this->belongsTo("App\Bride");
    }
}
