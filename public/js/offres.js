var scope = this;
budget = {
    bride :{
        coif_robe:{"0":"Inférieur à 1000dt", "1":"Entre 1000dt et 1500dt", "2":"Supérieur à 1500"},
        robe:{"0":"Inférieur à 800dt", "1":"Entre 800dt et 1500dt", "2":"Supérieur à 1500"},
        coif:{"0":"Entre 600 et 750"}
    },
    wtiya :{
        coif_robe :{"0":"Inférieur à 500dt", "1":"Entre 500dt et 1000dt", "2":"Supérieur à 1000"},
        robe :{"0":"Inférieur à 300dt", "1":"Entre 300dt et 500dt", "2":"Supérieur à 500"},
        coif :{"0":"400dt"}
    }
};



$(document).ready(function(){
    $("input[type=checkbox]").change(function(){
        var count = 0;
        var type = $(this).attr("class");
        var choice = "", message = "";
        $('#'+type+'_budget option:gt(0)').remove();
        $("#"+type+"_label").children().empty();
        $("input[type=checkbox][class="+type+"]:checked").each(function(){
            count ++;
            choice += this.name.split("_")[0] + "_";
            message += $(this).parent().text() + ",";
        });
        message.trim();
        message = message.replace(/\,$/, '');
        $("#"+type+"_label").append($("<small></small>").text("("+message+")"));
        choice = choice.replace(/\_$/, '');
        $.each(scope["budget"][type][choice], function(key, value){
            $("#"+type+"_budget").append($("<option></option>")
            .attr("value", value).text(value));
        });

        if(count)
            $("#"+type+"_budget")[0].selectedIndex = 1;
        else
            $("#"+type+"_budget")[0].selectedIndex = 0;
    });
});
