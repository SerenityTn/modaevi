
$(document).ready(function(){
    $("#mannequin-form").submit(function(e){
        var form = this;
        e.preventDefault();
        var data = $(this).serialize();
        xp_shoot = $("[name='xp_shoot']:checked").val();
        xp_def = $("[name='xp_def']:checked").val();
        data += "&xp_shoot=" + (xp_shoot == null ? "":xp_shoot);
        data += "&xp_def=" + (xp_def == null ? "":xp_def);
        data += "&portrait0=" + $("[name='portrait0']").val();

        $.ajax({
            type: "POST",
            url: $("#data-check").val(),
            data: data,
            statusCode: {
                400:function(response){
                    var element = null;
                    result = $.parseJSON(response.responseText);
                    for(var property in result.errors){
                        if(result.errors.hasOwnProperty(property)){
                            obj = $("[name="+property+"]");
                            if(element == null) element = $("[name="+property+"]");
                            if(obj.is(':radio'))
                                obj.parent().parent().css("border","1px solid red");
                            else if(obj.attr('type') == "file"){
                                if(!obj.parent().find('small').length)
                                    obj.after('<small class="text-danger">Veuillez selectionner une image</small>');
                            }else
                                obj.css("border","1px solid red");

                            obj.closest("div[class=form-group]").children("small").empty();
                            obj.closest("div[class=form-group]").append($("<small class='text-danger'></small>").text(result.errors[property]));
                        }
                    }
                    scrollToElement(element);
                },
                200:function(response){
                    form.submit();
                }
            }
        });
    });

    function check_data(element){
        var data = {};
        data[element.name] = element.value;
        var input = element;
        $.ajax({
            type: "POST",
            url: $("#data-check").val(),
            data: data,
            statusCode: {
                400:function(response){
                    result = $.parseJSON(response.responseText);
                    if($(input).is(':radio'))
                        $(input).parent().parent().css("border","1px solid red");
                    else
                        $(input).css("border","1px solid red");
                    $(input).closest("div[class=form-group]").children("small").empty();
                    $(input).closest("div[class=form-group]").append($("<small class='text-danger'></small>").text(result.errors[input.name]));
                },
                200:function(){
                    if($(input).is(':radio'))
                        $(input).parent().parent().css("border","1px solid green");
                    else
                        $(input).css("border","1px solid green");
                    $(input).closest("div[class=form-group]").children("small").empty();
                }
            }
        });
    }

    $("input:not([type=checkbox]), select").change(function(){
        check_data(this);
    });
});
