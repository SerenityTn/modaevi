$(document).ready(function(){
    $("#datetimepicker1").on("dp.change", function (e) {
        check_data(this);
    });


    $("input:not([type=checkbox]), select").change(function(){
            check_data(this);
    });

    $("#bride-form").submit(function(e){
        var form = this;
        e.preventDefault();
        var data = $(this).serialize();
        data += "&personal_img="+$("input[name=personal_img]").val();
        visited_us = $("[name='visited_us']:checked").val();
        data += "&visited_us=" + (visited_us == null ? "":visited_us);
        data += "&bride_budget=" + $("#bride_budget").val();
        data += "&wtiya_budget=" + $("#wtiya_budget").val();
        $.ajax({
            type: "POST",
            url: $("#data-check").val(),
            data: data,
            statusCode: {
                400:function(response){
                    var element = null;
                    result = $.parseJSON(response.responseText);
                    for(var property in result.errors){
                        if(result.errors.hasOwnProperty(property)){
                            obj = $("[name="+property+"]");
                            if(element == null) element = obj;
                            if(obj.is(':radio'))
                                obj.parent().parent().css("border","1px solid red");
                            else if(obj.attr('type') == "file"){
                                if(!obj.parent().find('small').length)
                                    obj.after('<small class="text-danger">Veuillez selectionner une image</small>');
                            }else
                                obj.css("border","1px solid red");

                            $("[name="+property+"]").closest("div[class=form-group]").children("small").empty();
                            $("[name="+property+"]").closest("div[class=form-group]").append($("<small class='text-danger'></small>").text(result.errors[property]));
                        }
                    }
                    scrollToElement(element);
                },
                200:function(){
                    form.submit();
                }
            }
        });
    });
});

function check_data(element){
    var data = {};
    data[element.name] = element.value;
    var input = element;
    $.ajax({
        type: "POST",
        url: $("#data-check").val(),
        data: data,
        statusCode: {
            400:function(response){
                result = $.parseJSON(response.responseText);
                $(input).css("border","1px solid red");
                $(input).closest("div[class=form-group]").children("small").empty();
                $(input).closest("div[class=form-group]").append($("<small class='text-danger'></small>").text(result.errors[input.name]));
            },
            200:function(){
                $(input).css("border","1px solid green");
                $(input).closest("div[class=form-group]").children("small").empty();
            }
        }
    });
}
