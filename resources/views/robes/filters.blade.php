<input type="hidden" value="{{ auth()->user()->id }}" id="user_id"/>
<div style="">
    <div class="col-md-2">
        <h5>Reférence :</h5>
        <input type="text" placeholder="chercher une robe" class="form-control" name="robe_ref"/>
    </div>
    <div class="col-md-4">
        <h5>Catégorie :</h5>
        @include("robes.categories.checkbox")
    </div>
    <!---<div class="col-md-2">
        <h5>Etat robe :</h5>
        {{--@include("robes.categories.radio", App\Robe::getList("etats", "state", [], true))--}}
    </div>!-->
    <div class="col-md-3">
        <h5>Robe de:</h5>
        @include("robes.categories.radio", App\Robe::getList("locals", "local", [], true))
    </div>
    <div class="col-md-3">
        <h5>Actuellement à :</h5>
        @include("robes.categories.radio", App\Robe::getList("locals", "current_local", [], true))
    </div>
</div>
@section('scripts')
    @parent
    <script type="text/javascript">
    $(document).ready(function(){
        function getCategories(){
            var categories = [];
            $('input[type=checkbox]:checked').each(function(){
                categories.push($(this).val());
            });
            return categories;
        }

        if($("#user_id").val() == 3)
            $("input[name=local]").eq(2).prop('checked', true);
        else
            $("input[name=local]").eq(1).prop('checked', true);

        $('input[name=robe_ref]').on('keyup', function(){
            filter_robes();
        });

    	$('.category_filter').click(function() {
            filter_robes();
    	});

        $('input[type=radio]').change(function(){
            filter_robes();
        });



        function filter_robes(){
            _categories = getCategories();
            _state = $('input[type=radio][name=state]:checked').val();
            _local = $('input[type=radio][name=local]:checked').val();
            _currentLocal = $('input[type=radio][name=current_local]:checked').val();
            _ref = $('input[name=robe_ref]').val();
            filters = {};

            if(_categories.length != 0) filters.categories = _categories;
            if(_state != -1) filters.state = _state;
            if(_local != -1) filters.local = _local;
            if(_currentLocal != -1) filters.current_local = _currentLocal;
            if(_ref != "") filters.ref = _ref;

            $.post(rootUrl + "admin/robe/filter", filters)
            .done(function(html){
                console.log(html);
                $('#list').html(html);
            });
        }
    });
    </script>
@stop
