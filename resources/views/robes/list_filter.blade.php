<?php
$selected_id = 0;
if(isset($robe))
    $selected_id = $robe->category;
?>
@include('robes.categories.radio', App\Robe::getList('categories', 'categories', [$selected_id]))

<div class="form-group" id="robes_by_list">
    {!! Form::label('robe_id', 'Robe') !!}
    {!! Form::select('robe_id', $robes_list, isset($robe) ? $robe->id:null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[type=radio]').change(function(){
                var _category_id = this.value;
                console.log(this.value);
                $.post(rootUrl + 'admin/robe/filter_list', {category_id: _category_id}).done(function(html){
                    $('#robes_by_list').html(html);
                });
            });
        });
    </script>
@stop
