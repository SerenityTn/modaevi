@if($default)
    <label class="radio-inline"><input type="radio" value="-1" checked name="{{ $name }}">Tous</label>
@endif
@foreach ($categories as $id => $categorie)
    @if(in_array($id, $selected_values))
        <label class="radio-inline"><input type="radio" checked value="{{ $id }}" name="{{ $name }}">{{ $categorie }}</label>
    @else
        <label class="radio-inline"><input type="radio" value="{{ $id }}" name="{{ $name }}">{{ $categorie }}</label>
    @endif
@endforeach
