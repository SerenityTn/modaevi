<button type="button" onclick="changeLocal(this)" class="{{ $robe->current_local == 0 ? "S":"J" }}" style="width:36px;">{{ $robe->current_local == 0 ? "S":"J" }}</button>
<input type="hidden" class="robe_id" value="{{ $robe->id }}">
@section('scripts')
    <script type="text/javascript">
        function changeLocal(btn){
            robe_id = $(btn).next().val();
            $.get(rootUrl + 'admin/robe/update_current_local/'+robe_id).done(function(newLocal){
                $(btn).text(newLocal);
                $(btn).attr("class", newLocal);
                console.log(newLocal);
            });
        }
    </script>
    @parent
@stop
