<button type="button"><img src='{{ asset(URL::asset("imgs/icons/state" . $robe->etat . ".png")) }}' width="20" onclick="changeState(this)"/></button>
<input type="hidden" class="robe_id" value="{{ $robe->id }}">
@section('scripts')
    <script type="text/javascript">
        function changeState(img){
            robe_id = $(img).parent().next().val();
            console.log(robe_id);
            $.get(rootUrl + 'admin/robe/update_state/'+robe_id).done(function(newState){
                currentSrc = $(img).attr('src');
                newSrc = currentSrc.replace(/state\d/i, "state" + newState);
                $(img).attr('src', newSrc);
                console.log(newState);
            });
        }
    </script>
    @parent
@stop
