<div class="col-md-12 robes-stats">
    Robes en totale : {{ count($robes) }}
</div>

@foreach($robes as $robe)
    <div class="col-md-2 robe-preview">
        <div class="preview-control">
            <span class="robe-control left-control">
                <button type="button" value="{{ $robe->id }}" class="btn-control delete-btn">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
            </span>
            <div class="robe-control right-control">
                <span class="btn-control stadte-btn">
                    @include('robes.btns.state', [$robe])
                </span>
                <span class="btn-control local-btn">
                    @include('robes.btns.local', [$robe])
                </span>
            </div>
            <a href="{{ route('admin.robe.edit', [$robe->id]) }}">
                <img  src='{{ url("public/images/$robe->img_name/150/200") }}'>
            </a>
        </div>
        <h5>Référence : <b>{{ $robe->ref }}</b></h5>
        <h5>Catégorie : <b>{{ $robe->getCategoryName() }}</b></h5>
        <h5>Prix : <b>{{ $robe->price }}</b></h5>
        <a href="{{ route('admin.location.robe.create', $robe->id) }}" class="btn btn-success">Louer cette robe</a>
    </div>
@endforeach

@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).on('click', '.delete-btn', function(){
            var btn = this;
            $.ajax({
                url:'robe/' + $(this).val(),
                type: 'DELETE',
                success: function(result){
                    parent = $(btn).parent().parent().parent();
                    parent.remove();
                    console.log(result);
                }
            });
        });
    </script>
@stop
