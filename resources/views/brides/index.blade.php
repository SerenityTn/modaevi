@extends('admin.base');
@section("content")
    <small>total: {{ App\Bride::count() }} </small>
    <table class="table table-striped" style="background:white" id="bridesTable">
        <thead>
          <tr>
              <th>&nbsp;</th>
              <th>Nom et prénom</th>
              <th>Mariage le</th>
              <th>Téléphone</th>
              <th>Budget Mariage</th>
              <th>Budget Wtiya</th>
              <th>Date de création</th>
              <th>&nbsp;</th>
          </tr>
        </thead>
    </table>
@stop
@section('scripts')
    <script type="text/javascript">
        var locationTable;
        $(document).ready(function(){
            $.extend( $.fn.dataTable.defaults, {
                autoWidth : false,
                processing: true,
                serverSide: true,
                columns: [
                    { data: 'personal_img', name: 'personal_img' },
                    { data: 'name', name: 'name' },
                    { data: 'wedding_date', name: 'wedding_date' },
                    { data: 'phone', name: 'phone' },
                    { data: 'bride_budget', name: 'bride_budget' },
                    { data: 'wtiya_budget', name: 'wtiya_budget' },
                    { data: 'created_at', name: 'created_at'},
                    { data: 'control', name: 'action', orderable: false, searchable: false}
                ]
            } );

            locationTable = $('#bridesTable').DataTable({
                ajax: {
                    url: '{!! route('brides.data') !!}',
                }
            });
            $('#bridesTable').on('submit','#deleteForm', function(evt){
                if(!confirm("Voulez-vous vraiement supprimé cette mariée ?"))
                    evt.preventDefault();

            });
        });
    </script>
@stop
