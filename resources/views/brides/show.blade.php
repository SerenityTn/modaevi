@extends('admin.base')
@section('content')
<table class="table striped">
    <tr>
        <td colspan="2">
            <img src='{{ url("images/brides_imgs/$bride->personal_img/300") }}' alt="" />
        </td>
    </tr>
    <tr>
        <th>Nom et prénom</th>
        <td> {{ $bride->name }}</td>
    </tr>
    <tr>
        <th>Région</th>
        <td>{{ $cities[$bride->city] }}</td>
    </tr>
    <tr>
        <th>Date de mariage</th>
        <td>{{ $bride->wedding_date }}</td>
    </tr>
    <tr>
        <th>Taille</th>
        <td>{{ $bride->size }}</td>
    </tr>
    <tr>
        <th>Téléphone</th>
        <td>{{ $bride->phone }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{ $bride->email }}</td>
    </tr>
    <tr>
        <th>Choix d'offre</th>
        <td>
            @foreach($bride->prefs as $pref)
                {{ $offres[$pref->label] }} <br/>
            @endforeach
        </td>
    </tr>
    <tr>
        <th>Budget Mariée</th>
        <td>{{ $bride->bride_budget }}</td>
    </tr>
    <tr>
        <th>Budget Wtiya</th>
        <td>{{ $bride->wtiya_budget }}</td>
    </tr>
    <tr>
        <th>Remarques</th>
        <td>{{ $bride->notice }}</td>
    </tr>
    <tr>
        <th>Nous a déja visité</th>
        <td>{{ $bride->visited_us ? "Oui":"Non"}}</td>
    </tr>
    <tr>
        <th>Coiffure préféré</th>
        <td>
            @foreach($bride->imgs as $img)
                @if($img->type == 0)
                    <img src="{{ url("images/brides_imgs/$img->name/300") }}"  alt="" />
                @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <th>Make up préférés</th>
        <td>
            @foreach($bride->imgs as $img)
                @if($img->type == 1)
                    <img src="{{ url("images/brides_imgs/$img->name/300") }}" alt="" />
                @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <th>Robes préférées</th>
        <td>
            @foreach($bride->imgs as $img)
                @if($img->type == 2)
                    <img src="{{ url("images/brides_imgs/$img->name/300") }}" alt="" />
                @endif
            @endforeach
        </td>
    </tr>
</table>
@stop
