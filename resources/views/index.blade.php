@extends('base')
@section('menu')
    @include('partials/menu')
@stop
@section('slider')
    @include('partials/slider')
@stop
@section('content')
    <a href="{{ url('fr/bride-form/create') }}" class="btn btn-success">Formulaire Mariées</a>
    <a href="{{ url('fr/mannequin-form/create') }}" class="btn btn-primary">Formulaire Mannequin</a>
@stop
