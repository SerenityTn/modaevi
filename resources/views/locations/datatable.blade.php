<table class="table table-striped" style="background:white" id="locationTable">
    <thead>
      <tr>
        <th>Insérer par</th>
        <th>Cliente</th>
        <th>Robe</th>
        <th >Payée</th>
        <th>Reste</th>
        <th style="width:60px">Date</th>
        <th>Ajoutée le</th>
        <th>Assurance</th>
        <th>Notes</th>        
        <th>Etat</th>
      </tr>
    </thead>
</table>
@section('scripts')
    <script type="text/javascript">
        var locationTable;
        var dateColIndex = 5;
        $(document).ready(function(){
            $.extend( $.fn.dataTable.defaults, {
                autoWidth : false,
                processing: true,
                serverSide: true,
                searching:false,
                columns: [
                    { data: 'username', name: 'users.name' },
                    { data: 'name', name: 'clientes.name' },
                    { data: 'ref', name: 'robes.ref' },
                    { data: 'avance', name: 'avance' },
                    { data: 'reste', name: 'reste' },
                    { data: 'date_location', name: 'date_location' },
                    { data: 'created_at', name: 'created_at',order: 'desc' },
                    { data: 'assurance', name: 'assurance' },
                    { data: 'notes', name: 'notes' },
                    { data: 'etat', name: 'robes.etat'},
                    { data: 'control', name: 'action', orderable: false, searchable: false}
                ],
                order: [[ 6, "desc" ]]
            } );

            $('#locationTable').on('submit','#deleteForm', function(evt){
                if(!confirm("Voulez-vous vraiement supprimé cet élément ?"))
                    evt.preventDefault();

            });

            $('#locationTable').on('click','.state', function(evt){
                img = evt.target;
                tr = $(img).closest('tr')[0];
                console.log(locationTable.row(tr).data());
                robe_id = locationTable.row(tr).data()["robe_id"];
                $.get(rootUrl + 'admin/robe/update_state/'+robe_id).done(function(newState){
                    currentSrc = $(img).attr('src');
                    newSrc = currentSrc.replace(/state\d/i, "state" + newState);
                    $(img).attr('src', newSrc)
                });
            });
        });
    </script>
    @parent
@stop
