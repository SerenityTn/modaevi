@extends('admin.base')
@section('content')
    <div class="col-sm-4 col-sm-offset-4">
        {!! Form::model($location,['route' => ['admin.location.update', $location->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'files' => 'true']) !!}
            <div class="form-group{{ $errors->has('cliente_id') ? ' has-error' : '' }}">
                {!! Form::label('cliente_id', 'Cliente') !!}
                {!! Form::select('cliente_id', $clientes_list, $location->cliente->id, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('cliente_id') }}</small>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label  class="checkbox-inline">
                        <input type="checkbox" name="coif_only" {{ $location->robe->ref == -1 ? "checked":"" }}>Coiffure seulement
                    </label>
                </div>
                <small class="text-danger">{{ $errors->first('coif_only') }}</small>
            </div>

            <div id="robe_chooser">
                @include('robes.list_filter', ['robe' => $location->robe])
            </div>

            @include('partials.forms.addons', ['location_addons' => $location->addons])

            <div class="form-group{{ $errors->has('avance') ? ' has-error' : '' }}">
                {!! Form::label('avance', "Avance payée") !!}
                {!! Form::text('avance', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('avance') }}</small>
            </div>

            <div class="form-group{{ $errors->has('reste') ? ' has-error' : '' }}">
                {!! Form::label('reste', "Montant restant") !!}
                {!! Form::text('reste', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('reste') }}</small>
            </div>

            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    {!! Form::text('date_location', $location->date_location, ['class' => 'form-control', 'required' => 'required']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>

             <div class="form-group">
                 {!! Form::label('assurance', "Assurance") !!}
                 {!! Form::text('assurance', null, ['class' => 'form-control']) !!}
             </div>
            <div class="form-group">
                {!! Form::label('notes', "Remarques") !!}
                {!! Form::text('notes', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('event', "Evènement") !!}
                {!! Form::text('event', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit("Modifier la location", ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("input[name=coif_only]").click(function(){
                coifOnly = $(this).is(":checked");
                if(coifOnly){
                    $('input[type=radio]')[0].checked = true;
                    $($('input[type=radio]')[0]).trigger("change");
                    $("#robe_chooser").hide();
                }else{
                    $("#robe_chooser").show();
                }
            });
            $("input[name=coif_only]").trigger("click");
            $("input[name=coif_only]").trigger("click");
        });
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });
        });
    </script>
@stop
