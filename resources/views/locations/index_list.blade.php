@include('locations.datatable')
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            locationTable = $('#locationTable').DataTable({
                ajax: {
                    type: 'POST',
                    url: '{!! route('locationDatatable.filter') !!}',
                    data: function (d) {
                        begin_date = $('input[name=begin_date]').val();
                        d.begin_date = begin_date;

                        end_date = $('input[name=end_date]').val();
                        if(end_date) d.end_date = end_date;

                        var checked_areas =  $('input[name="area[]"]:checked');

                        if(checked_areas.length == 1)
                            d.areas = $(checked_areas[0]).val();

                        d.robe = $('input[name=robe]').val();
                        d.name = $('input[name=name]').val();
                    }
                },
            });

            $("select[name=time_interval]").change(function(){
                if($(this).val() == ""){
                    $("#begin_date").val(moment().format("YYYY-MM-DD"));
                    $("#end_date").val(null);
                }else if($(this).val() == -1){
                    $("#begin_date").val(moment().add($(this).val(), 'day').format("YYYY-MM-DD"));
                    $("#end_date").val(moment().add($(this).val(), 'day').format("YYYY-MM-DD"));
                }else{
                    $("#begin_date").val(moment().format("YYYY-MM-DD"));
                    $("#end_date").val(moment().add($(this).val(), 'day').format("YYYY-MM-DD"));
                }


                locationTable.draw();
            });

            $('input[name="area[]"]').on('change', function(){
                locationTable.draw();
            });            

            $('input[name=robe]').on('keyup', function(){
                locationTable.draw();
            });


            $('#begin_picker, #end_picker').on('dp.change', function(){
                locationTable.draw();
            });

            $('input[name=name]').on('keyup change', function () {
                locationTable.draw();
           });
        });
    </script>
@stop
