@include('locations.datatable')
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            locationTable = $('#locationTable').DataTable({
                ajax: {
                    type: 'POST',
                    url: '{!! route('locationDatatable.filter') !!}',
                    data: function (d) {
                        d.begin_date = moment().format("YYYY-MM-DD");
                        days = $('select[name=days]').val();
                        d.end_date = moment().add(days, 'day').format("YYYY-MM-DD");
                    }
                },                
                columnDefs: [
                    { "visible": false, "targets": [2]},
                    { "visible": false, "targets": [5]}
                ]
            });

            $('#begin_picker, #end_picker').on('dp.change', function(e){
                locationTable.draw();
                e.preventDefault();
            });

            $("locationTable").attr("width", 100);
        });
    </script>
@stop
