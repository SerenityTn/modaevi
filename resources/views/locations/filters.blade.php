<div class="col-md-2 form-group">
        <input type="text" class="form-control" name="name"  placeholder="Nom et/ou Prénom"/>
        <div class="checkbox">
            <label><input type="checkbox" name="area[]" {{ auth()->user()->name == "Ferdaws" ? "checked":"" }} value="2" /> Sousse</label>&nbsp;&nbsp;&nbsp;
            <label><input type="checkbox" name="area[]" {{ auth()->user()->name == "Zouhour" ? "checked":"" }} value="3" /> Jemmel</label>
        </div>
</div>
<div class="col-md-2 form-group">
        <input type="text" class="form-control" name="robe"  placeholder="Référence robe"/>
</div>
<div class="col-md-3 form-group">
    <div class='input-group' id="begin_picker" style="padding-bottom:5px;">
        <input type='text' id='begin_date' value="{{ Carbon\Carbon::today() }}" class="form-control" name="begin_date" placeholder="Date de location initiale"/>
        <span class="input-group-addon clearField">
            <a href="#" style="color:red"><span class="glyphicon glyphicon-remove"></span></a>
        </span>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <div class='input-group' id="end_picker">
        <input type='text' id='end_date' value="" class="form-control" name="end_date" placeholder="Date de location finale"/>
        <span class="input-group-addon clearField" >
            <a href="#" style="color:red"><span class="glyphicon glyphicon-remove"></span></a>
        </span>
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
</div>

<div class="col-md-5 form-group">
    @include('forms.select', App\Location::getList('time_intervals', 'time_interval'))
</div>

@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            $(".clearField").on("click", function(e){
                prev = $(this).prev();
                console.log(prev);
                $(prev).val('');
                $(prev).change();
            });
        });

        $("#begin_picker").datetimepicker({
            format: "YYYY-MM-DD HH:mm"
        });
        $("#end_picker").datetimepicker({
            format: "YYYY-MM-DD HH:mm"
        });

        $("#begin_picker").on("dp.change", function (e) {
            $('#end_picker').data("DateTimePicker").minDate(e.date);
        });

        $("#end_picker").on("dp.change", function (e) {
            $('#begin_picker').data("DateTimePicker").maxDate(e.date);
        });
    </script>
@stop
