    @extends('public')
@section('content')
    <?php
        $selected_value = "";
        function offertype($offer){
            $arr = explode("_", $offer);
            return array_pop($arr);
        }
     ?>

        <div class="col-md-1">
            <a href="{{ url("ar/bride-form/create") }}">العربية</a><br/>
            <a href="{{ url("fr/bride-form/create") }}">Français </a>
        </div>
        {!! Form::open(['method' => 'POST', 'route' => 'bride-form.store', 'files' => 'true', 'id' => 'bride-form']) !!}
        <input type="hidden" id="data-check" value="{{ route('bride-form.check') }}"/>
        <div class="col-md-8 col-md-offset-1">
            <div class="panel-group">
                <div class="col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">1. Vos informations personnelles</div>
                        <div class="panel-body">
                            <div class="form-group">
                            {!! Form::label('cin', trans('validation.attributes.cin').'(*)') !!}
                            {!! Form::text('cin', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                            {!! Form::label('name', trans('validation.attributes.name').'(*)') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                            {!! Form::label('size', trans('validation.attributes.size')) !!}
                            {!! Form::text('size', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">2. Contact</div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('email', trans('validation.attributes.email').'(*)') !!}
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('phone', trans('validation.attributes.phone')."(*)") !!}
                                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('city', trans('validation.attributes.city')."(*)") !!}
                                {!! Form::select('city', $cities, $selected_value, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            &nbsp;
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-group">
                <div class="col-md-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">3. Détails du mariage</div>
                        <div class="panel-body">
                            <div class="form-group col-md-6">
                                {!! Form::label('wedding_date', trans('validation.attributes.wedding_date')."(*)") !!}
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" name="wedding_date" placeholder="aaaa/mm/jj"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label>@lang("validation.attributes.visited_us")</label><br/>
                                <div class="radio">
                                    <label><input type="radio" name="visited_us" value="1"/>Oui</label>&nbsp;&nbsp;&nbsp;
                                    <label><input type="radio" name="visited_us" value="0"/>Non, pas encore</label><br/>
                                    <small id="note">Vous devriez visiter notre espace pour confirmer votre inscription<br/>
                                    <b>Minimum avance: 100DT pour coiffure</b><br/>
                                    <b>Minimum avance: 100DT pour robe</b>
                                    </small>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label> @lang("validation.attributes.service")(*)</label>
                                @foreach($offres_label as $o)
                                    <div class="checkbox">
                                        <label><input type="checkbox" class="{{ offertype($o->label) }}" name="{{ $o->label }}" value="{{ $o->label }}">{{ trans("validation.choices.$o->label") }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('bride_budget', 'Quel est votre budget pour mariage', ['id'=>'bride_label']) !!}
                                <select class="form-control" name="bride_budget" id="bride_budget">
                                <option value="">Veuillez cocher une offre</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('wtiya_budget', 'Quel est votre budget pour wtiya', ['id'=>'wtiya_label']) !!}
                                <select class="form-control" name="wtiya_budget" id="wtiya_budget">
                                <option value="">Veuillez cocher une offre</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            &nbsp;
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-12">
                <div class="panel-group">
                    <div class="panel panel-danger">
                        <div class="panel-heading">4. Ajouter vos styles préférés !</div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                <label>@lang('validation.attributes.perso_img') (*)</label><br/>
                                @include("partials.img_box", ["name" => "personal_img"])
                            </div>
                            <div class="form-group col-md-12">
                                <label> @lang("validation.attributes.coif_img") <small> @lang('validation.attributes.choicer')</small></label><br/>
                                @for($i = 0; $i < 3; $i++)
                                    @include("partials.img_box", ["name" => "coif$i"])
                                @endfor
                            </div>
                            <div class="form-group col-md-12">
                                <label>@lang('validation.attributes.make_up_img') &nbsp;<small> @lang('validation.attributes.choicer')</small></label><br/>
                                @for($i = 0; $i < 3; $i++)
                                    @include("partials.img_box", ["name" => "make_up$i"])
                                @endfor
                            </div>
                            <div class="form-group col-md-12">
                                <label>@lang('validation.attributes.robe_img') <small> @lang('validation.attributes.choicer')</small></label><br/>
                                @for($i = 0; $i < 3; $i++)
                                    @include("partials.img_box", ["name" => "robe$i"])
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-12">
                <div class="panel-group">
                    <div class="panel panel-danger">
                        <div class="panel-heading">5. Remarques </div>
                        <div class="panel-body">
                            <input type="textarea" name="notice" class="form-control" placeholder="Vous pouvez ajouter des remarques ou des informations supplémentaires ici.."/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                {!! Form::submit("Inscrivez vous !", ['class' => 'btn btn-success']) !!}
            </div>
        </div>
    {!! Form::close() !!}
@stop
@section('scripts')
    @parent
    <script type="text/javascript" src="{{ URL::asset("js/offres.js") }}"></script>
    <script type="text/javascript" src="{{ URL::asset("js/data-check-bride.js") }}"></script>
    <script type="text/javascript">
        $(":file").filestyle({buttonText: "Choisir un fichier"});
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        function read(input, prev) {
            img = prev.children().eq(1);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function(){
            $("input[type=file]").change(function(){
                prev = $(this).prev();
                if($(this).val() != ""){
                    $(prev).show();
                    read(this, prev);
                    $(this).parent().find("small").remove();
                }else{
                    $(prev).hide();
                }
            });

            $("select").change(function(){
                if($(this).children().length > 1)
                    $(this).css('border', '1px solid green');
                else
                    $(this).css('border', '1px solid red');
            });

            $("input[type=checkbox]").change(function(){
                if($("input[type=checkbox]:checked").length > 0)
                    $("select").css('border', '1px solid green');
                else
                    $("select").css('border', '1px solid red');
            });


            $("#note").hide();
            $(".prev").hide();
            $("input[type=radio][name=visited_us]").change(function(){
                $(this).parent().parent().css('border','1px solid green');
                if(this.value == 0)
                    $("#note").show();
                else
                    $("#note").hide();

            });
            $(".delete-btn").click(function(){
                input = $(this).parent().next();
                console.log(input.val());
                input.val("");
                input.change();
            });
        });

        function scrollToElement(element){
            $('html, body').animate({
                scrollTop: element.offset().top - 80
            }, 1000);
        }
    </script>
@stop
