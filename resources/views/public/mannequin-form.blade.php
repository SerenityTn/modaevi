@extends('public')
@section('content')
    <?php $selected_value = "" ?>
    {!! Form::open(['method' => 'POST', 'route' => 'mannequin-form.store', 'files' => 'true', 'id' => 'mannequin-form']) !!}
    <input type="hidden" id="data-check" value="{{ route('mannequin-form.check') }}">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-group">
            <div class="col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">1. Vos informations personnelles</div>
                    <div class="panel-body">
                        <div class="form-group">
                        {!! Form::label('cin', trans('validation.attributes.cin').'(*)') !!}
                        {!! Form::text('cin', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                        {!! Form::label('name', trans('validation.attributes.name').'(*)') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-md-4 row">
                            <div class="form-group">
                            {!! Form::label('age', 'Age (*)') !!}
                            {!! Form::text('age', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            {!! Form::label('height', trans('validation.attributes.height')) !!}
                            {!! Form::text('height', null, ['class' => 'form-control', 'placeholder'=>'en cm']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 row">
                            <div class="form-group">
                            {!! Form::label('weight', 'Poids (*)') !!}
                            {!! Form::text('weight', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">2. Contact</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('email', trans('validation.attributes.email').'(*)') !!}
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('phone', trans('validation.attributes.phone')."(*)") !!}
                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('city', trans('validation.attributes.city')."(*)") !!}
                            {!! Form::select('city', $cities, $selected_value, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        &nbsp;
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-group">
            <div class="col-md-12">
                <div class="panel panel-danger">
                    <div class="panel-heading">3. Expériences</div>
                    <div class="panel-body">
                        <div class="form-group col-md-6">
                            {!! Form::label('xp_shoot', "Est ce que vous avez une expérience en shooting ?(*)") !!}
                            <div class="radio form-group">
                                <label><input type="radio" name="xp_shoot"  value="1"/>Oui.</label>&nbsp;&nbsp;&nbsp;
                                <label><input type="radio" name="xp_shoot"  value="0"/>Non.</label><br/>
                            </div>
                            <input type="textarea" name="xp_shoot_txt" class="form-control" placeholder="Parler nous de votre expérience..."/>
                            <p class="help-block"><strong></strong> caractère(s) restant(s)</p>
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('xp_def', "Est ce que vous avez participer à un défilé ?(*)") !!}
                            <div class="radio">
                                <label><input type="radio" name="xp_def" value="1"/>Oui</label>&nbsp;&nbsp;&nbsp;
                                <label><input type="radio" name="xp_def" value="0"/>Non</label><br/>
                            </div>
                            <input type="textarea" name="xp_def_txt" class="form-control" placeholder="Parler nous de votre expérience..."/>
                            <p class="help-block"><strong></strong> caractère(s) restant(s)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        &nbsp;
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-12">
            <div class="panel-group">
                <div class="panel panel-danger">
                    <div class="panel-heading">4. Ajouter vos photos et CV</div>
                    <div class="panel-body">
                        <div class="form-group col-md-12">
                            <label>Photos portrait (minimum une photo)</label><br/>
                            <div class="row">
                                @for($i = 0; $i < 3; $i++)
                                    @include("partials.img_box", ["name" => "portrait$i"])
                                @endfor
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group">
                                {!! Form::label('cv_file', 'Votre CV ') !!} <small>(non obligatoire)</small>
                                {!! Form::file('cv_file') !!}
                                <p class="help-block">Vous pouvez joindre votre CV!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-12">
            <div class="panel-group">
                <div class="panel panel-danger">
                    <div class="panel-heading">5. Message </div>
                    <div class="panel-body">
                        <input type="textarea" name="notice" class="form-control" placeholder="Vous pouvez ajouter des remarques ou des informations supplémentaires ici.."/>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-md-12">
            {!! Form::submit("Inscrivez vous !", ['class' => 'btn btn-success']) !!}
        </div>
    </div>
{!! Form::close() !!}
@stop
@section('scripts')
@parent

<script type="text/javascript" src="{{ URL::asset('js/data-check-mannequin.js') }}"></script>
<script type="text/javascript">
    $(":file").filestyle({buttonText: "Choisir un fichier"});
    function read(input, prev) {
        img = prev.children().eq(1);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(img).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function(){
        //file preview

        $(".prev").hide();
        $(".delete-btn").click(function(){
            input = $(this).parent().next();
            input.val("");
            input.change();
        });

        $("input[type=file]").change(function(){
            prev = $(this).prev();
            if($(this).val() != ""){
                $(prev).show();
                read(this, prev);
                $(this).parent().find("small").remove();
            }else{
                $(prev).hide();
            }
        });


        $("input[name=xp_shoot], input[name=xp_def]").each(function(){
            txt = $(this).parent().parent().next();
            txt.hide();
            txt.next().hide();
        });

        $("input[name=xp_shoot], input[name=xp_def]").change(function(){
            txt = $(this).parent().parent().next();
            if($(this).val() == 1){
                txt.show();
                txt.next().show();
            }else{
                txt.val("");
                txt.hide();
                txt.next().hide();
            }
        });

        var xp_count = 40;
        $("strong").text(xp_count);
        $("input[type=textarea]").on('keyup', function(){
            if(xp_count - $(this).val().length > 0){
                $(this).next().show();
                $(this).next().children().first().text(xp_count - $(this).val().length);
            }else{
                $(this).next().hide();
            }
        });
    });
    function scrollToElement(element){
        $('html, body').animate({
            scrollTop: element.offset().top - 80
        }, 1000);
    }
</script>
@stop
