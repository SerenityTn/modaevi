@include('clientes.datatable')
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function(){
            locationTable = $('#locationTable').DataTable({
                ajax: {
                    url: '{!! route('clientes.data') !!}',
                }
            });
        });
    </script>
@stop
