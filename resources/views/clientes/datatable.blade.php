<table class="table table-striped" style="background:white" id="locationTable">
    <thead>
      <tr>
          <th>Nom et prénom</th>
          <th>Numéro 1</th>
          <th>Numéro 2</th>
          <th>Adresse</th>
          <th>Date de création</th>
          <th>&nbsp;</th>
      </tr>
    </thead>
</table>
@section('scripts')
    <script type="text/javascript">
        var locationTable;
        var dateColIndex = 5;
        $(document).ready(function(){
            $.extend( $.fn.dataTable.defaults, {
                autoWidth : false,
                processing: true,
                serverSide: true,
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'number1', name: 'number1' },
                    { data: 'number2', name: 'number2' },
                    { data: 'address', name: 'address' },
                    { data: 'created_at', name: 'created_at'},
                    { data: 'control', name: 'action', orderable: false, searchable: false}
                ]
            } );
        });
    </script>
    @parent
@stop
