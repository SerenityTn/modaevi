@extends('admin.base')
@section('content')
<?php
    $name = $mannequin->imgs()->where("type","0")->first()->name;
    $path = url('images/mannequins_imgs/'. $name.'/150');
?>

<table class="table striped">
    <tr>
        <td colspan="2">
            <img src='{{ $path }}' alt="" />
        </td>
    </tr>
    <tr>
        <th>Nom et prénom</th>
        <td> {{ $mannequin->name }}</td>
    </tr>
    <tr>
        <th>Age</th>
        <td>{{ $mannequin->age }}</td>
    </tr>
    <tr>
        <th>Taille</th>
        <td>{{ $mannequin->height }}</td>
    </tr>
    <tr>
        <th>Poids</th>
        <td>{{ $mannequin->weight }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{ $mannequin->email }}</td>
    </tr>
    <tr>
        <th>Téléphone</th>
        <td>{{ $mannequin->phone }}</td>
    </tr>
    <tr>
        <th>Région</th>
        <td>{{ $cities[$mannequin->city] }}</td>
    </tr>
    <tr>
        <th>Expérience shooting</th>
        <td>{{ empty($mannequin->xp_shoot_txt) ? "Pas d'expérience" :  $mannequin->xp_shoot_txt }}</td>
    </tr>
    <tr>
        <th>Expérience défilé</th>
        <td>{{ empty($mannequin->xp_def_txt) ? "Pas d'expérience" : $mannequin->xp_def_txt }}</td>
    </tr>
    <tr>
        <th>CV</th>
        <td>
            @if(!empty($mannequin->cv_file))
                <a href='{{ route("download", ["name" => $mannequin->cv_file])  }}'>Télécharger</a>
            @else
                Pas de CV
            @endif
        </td>
    </tr>
    <tr>
        <th>Messages</th>
        <td>{{ $mannequin->notice }}</td>
    </tr>
    <tr>
        <th>Protrait</th>
        <td>
            @foreach($mannequin->imgs as $img)
                @if($img->type == 0)
                    <img src="{{ url("images/mannequins_imgs/$img->name/300") }}"  alt="" />
                @endif
            @endforeach
        </td>
    </tr>
</table>
@stop
