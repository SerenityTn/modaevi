@extends('admin.base');
@section("content")
    <small>total: {{ App\Mannequin::count() }} </small>
    <table class="table table-striped" style="background:white" id="mannequinsTable">
        <thead>
          <tr>
              <th>&nbsp;</th>
              <th>Nom et prénom</th>
              <th>Age</th>
              <th>Taille</th>
              <th>Poids</th>              
              <th>Expérience shooting</th>
              <th>Expérience Défilé</th>
              <th>CV</th>
              <th>Date inscription</th>
              <th>&nbsp;</th>
          </tr>
        </thead>
    </table>
@stop
@section('scripts')
    <script type="text/javascript">
        var locationTable;
        $(document).ready(function(){
            $.extend( $.fn.dataTable.defaults, {
                autoWidth : false,
                processing: true,
                serverSide: true,
                columns: [
                    { data: 'personal_img', name: 'personal_img',orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'age', name: 'age' },
                    { data: 'height', name: 'height' },
                    { data: 'weight', name: 'weight' },
                    { data: 'xp_shoot_txt', name: 'xp_shoot_txt' },
                    { data: 'xp_def_txt', name: 'xp_def_txt' },
                    { data: 'cv_file', name: 'cv_file' },
                    { data: 'created_at', name: 'created_at'},
                    { data: 'control', name: 'action', orderable: false, searchable: false}
                ]
            } );

            locationTable = $('#mannequinsTable').DataTable({
                ajax: {
                    url: '{!! route('mannequins.data') !!}',
                }
            });
            $('#mannequinssTable').on('submit','#deleteForm', function(evt){
                if(!confirm("Voulez-vous vraiement supprimé cette mannequin ?"))
                    evt.preventDefault();
            });
        });
    </script>
@stop
