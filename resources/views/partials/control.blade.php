{{ Form::open(['route' => ["admin.".strtolower(class_basename($model)).".destroy", $model->id], 'method' => 'delete', 'class' => 'form-inline', 'style' => 'display:inline', 'id' => 'deleteForm']) }}
<a href="{{ route('admin.'.$type.'.show', [$model->id]) }}"><span class="glyphicon glyphicon-eye-open"></span></a>
<button type="submit" class="model-control"><span class="glyphicon glyphicon-remove"></span></button>
{{ Form::close() }}
