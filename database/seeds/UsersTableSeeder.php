<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $user = User::create(['name' => 'Serenity', 'email' => 'serenity1994@hotmail.fr', 'password' => bcrypt("aspirine")]);
        $user = User::create(['name' => 'Ferdaws', 'email' => 'ferdaws@gmail.com', 'password' => bcrypt("123ferdaws")]);
        $user = User::create(['name' => 'Zouhour', 'email' => 'zouhour@gmail.com', 'password' => bcrypt("zouhour123")]);
    }
}
