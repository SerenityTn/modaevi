<?php

use Illuminate\Database\Seeder;

use App\Role;

class RolesTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Role::create(['label' => 'manage_forms']);
        Role::create(['label' => 'view_prices']);
        $serenity = App\User::find(1);
        $roles =  App\Role::all();
        $serenity->roles()->attach($roles);
    }
}
