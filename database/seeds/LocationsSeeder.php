<?php

use Illuminate\Database\Seeder;

use App\Location;
use App\Cliente;
use App\Robe;

class LocationsTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $xml = simplexml_load_file(storage_path("db.xml"));

        $i = 0;
        foreach($xml as $location_xml){
            $cliente = new Cliente();
            $cliente->name = $location_xml->prenom . " " . $location_xml->nom;
            $nbs = explode('/', $location_xml->num);
            $cliente->number1 = $nbs[0];
            if(count($nbs) > 1) $cliente->number2 = $nbs[1];
            $cliente->save();
            echo 'cliente '. $cliente->name ." saved! \n";
            $location = new Location();
            $location->user_id = 3;
            $location->cliente_id = $cliente->id;
            echo "looking for robe ". $location_xml->robe_id . "\n";
            if(!($robe = Robe::where('ref', $location_xml->robe_id)->where('category',0)->get()->first())){
                /*echo "robe ". $location_xml->robe_id . " non trouvé \n ajouter cette robe ? \n";
                $handle = fopen ("php://stdin","r");
                $line = fgets($handle);
                if(trim($line) == 'y'){*/
                    $robe = Robe::create([
                        'ref' => $location_xml->robe_id, 'category' => 0, 'img_name' => 'robe0.jpg',
                        'local' => 1, 'current_local' => 1
                    ]);
                    echo "robe ". $robe->ref ." saved! \n";
                //}
            }
            $location->robe_id = $robe->id;
            $location->avance = $location_xml->avance_paye;
            $location->reste = $location_xml->reste_paye;
            $location->date_location = $location_xml->date_mariage;
            $location->notes = $location_xml->desc;
            $location->save();
            echo "location saved ! \n";
        }
    }
}
