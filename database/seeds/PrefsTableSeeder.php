<?php

use Illuminate\Database\Seeder;

use App\Pref;

class PrefsTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Pref::create(['label' => 'coif_bride']);
        Pref::create(['label' => 'robe_bride']);
        Pref::create(['label' => 'coif_wtiya'] );
        Pref::create(['label' => 'robe_wtiya'] );
        Pref::create(['label' => 'hijab'] );
    }
}
