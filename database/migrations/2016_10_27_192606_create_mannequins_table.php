<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMannequinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('mannequins', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('cin');
            $table->string('name');
            $table->integer('age');
            $table->integer('height');
            $table->integer('weight');
            $table->string('email');
            $table->string('phone');
            $table->string('city');
            $table->text('xp_def_txt');
            $table->text('xp_shoot_txt');
            $table->string('cv_file');
            $table->text('notice');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::drop('mannequins');
    }
}
