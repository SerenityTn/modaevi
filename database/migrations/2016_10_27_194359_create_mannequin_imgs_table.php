<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMannequinImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('mannequin_imgs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('mannequin_id')->unsigned();
            $table->string('name');
            $table->integer('type');
            $table->timestamps();
            $table->foreign('mannequin_id')->references('id')->on('mannequins')->onDelete('cascade');
        });
    }

    public function down(){
        Schema::table('mannequin_imgs', function(Blueprint $table){
            $table->dropForeign(['mannequin_id']);
        });
        Schema::drop('mannequin_imgs');
    }
}
