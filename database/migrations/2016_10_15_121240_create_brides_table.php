<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBridesTable extends Migration{
    public function up(){
        Schema::create('brides', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();;
            $table->string('cin');
            $table->string('name');
            $table->date('wedding_date');
            $table->string('city');
            $table->integer('size');
            $table->string('phone');
            $table->string('email');
            $table->string('bride_budget');
            $table->string('wtiya_budget');
            $table->boolean('visited_us');
            $table->string('personal_img');
            $table->text('notice');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::drop('brides');
    }
}
