<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('imgs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('bride_id')->unsigned();
            $table->string("name");
            $table->integer("type");
            $table->foreign('bride_id')->references('id')->on('brides')->onDelete('cascade');
            $table->timestamps();
        });
    }


    public function down(){
        Schema::table('imgs', function(Blueprint $table){
            $table->dropForeign(['bride_id']);
        });
        Schema::drop('imgs');
    }
}
