<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prefs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('label');
            $table->timestamps();
        });

        Schema::create('bride_pref', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('bride_id')->unsigned();
            $table->integer('pref_id')->unsigned();
            $table->foreign ( "bride_id" )->references ( 'id' )->on ( 'brides' )->onDelete('cascade');
            $table->foreign ( "pref_id" )->references ( 'id' )->on ( 'prefs' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('bride_pref', function(Blueprint $table){
            $table->dropForeign(['bride_id']);
            $table->dropForeign(['pref_id']);
        });
        Schema::drop('bride_pref');
        Schema::drop('prefs');
    }
}
